/**
 * shows error message
 * @param divName - div where need to insert message
 * @param message - message body
 */
function showErrorAlert(divName, message){
    var div = $('#' + divName);
    var html = "<div class=\"alert fade in\">";
    html += "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">�</button>";
    html += message + "</div>";
    div.html(html);
}

/**
 * shows success message
 * @param divName - div where need to insert message
 * @param message - message body
 */
function showSuccessMsg(divName, message){
    var div = $('#' + divName);
    var html = "<div class=\"alert-success fade in\" style=\"padding: 10px;\" >";
    html += "<button type=\"button\" class=\"close\" onclick=\"hideSuccessAlertMsg('" + divName + "')\" data-dismiss=\"alert\">�</button>";
    html += message + "</div>";
    div.html(html);
    div.modal('show');
}

/**
 * hides success message
 * @param divName - div name
 */
function hideSuccessAlertMsg(divName){
    var div = $('#' + divName);
    div.modal('hide');
}