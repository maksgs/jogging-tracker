var tableData = $('#joggingTable').dataTable({
    "bProcessing": true,
    "bServerSide": true,
    "bPaginate": true,
    "bFilter": true,
    "bInfo": true,
    "bAutoWidth" : true,
    "sAjaxSource": 'jogging/list',
    "aoColumnDefs": [
        {"aTargets": [5],
            "sType": "html",

            "mRender": function(recordId, data, row) {
                //build action column
                return buildActionColumn(recordId, row[0],  row[1], row[6], row[4]);
            }
        }
    ],
    "aoColumns": [
        {"bSortable": true},
        {"bSortable": true},
        {"bSortable": true},
        {"bSortable": false},
        {"bSortable": true},
        {"bSortable": false}
    ]
}).columnFilter({
        sPlaceHolder: "head:before",
        aoColumns: [ null, null, null, null, null, { type: "date-range" } ]
    });

/**
 * builds action column
 * @param recordId - record id
 * @param date - date
 * @param time - time
 * @param distance - distance in meters
 * @param comment - comment in meters
 * @returns {string} - html code
 */
function buildActionColumn(recordId, date, time, distance, comment ){
    var html = "<div class=\"dropdown right\">";
    html += "<p class=\"dropdown-toggle\" data-toggle=\"dropdown\">";
    html += "Actions <b class=\"caret\"></b></p>";
    html += "<ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"dLabel\">";
    html += "<li><a onclick=\"showForm(" + recordId +",'" + date + "', '" + time + "','" + distance + "','" + comment + "')\" >Edit</a></li>";
    html += "<li><a onclick=\"showRemoveForm("+ recordId + " )\">Remove</a></li>";
    html += "</ul></div>";
    return html;
}

/**
 * shows expense form
 */
function showForm(recordId, date, time, distance, comment){
    var saveUrl;
    var saveMethod;
    $('#recordId').val(recordId);
    if (recordId == null || recordId == undefined){
        $('#timepicker').val("00:00:01");
        $('#distance').val(0);
        $('#comment').val('');
        $('#dateInput').val(getCurrentDate());
        saveUrl = 'jogging';
        saveMethod = 'POST';
    }else{
        $('#timepicker').val(time);
        $('#distance').val(distance);
        $('#comment').val(comment);
        $('#dateInput').val(date);
        saveUrl = 'jogging/' + recordId +'.json';
        saveMethod = 'PUT';
    }
    $('#save_errors').html('');
    //reset form
    $("#jogging-form").ajaxForm({
        url: saveUrl,
        type: saveMethod,
        success: function(data){
            if (data.success == 'true'){
                $('#joggingFormModal').modal('hide');
                tableData.fnClearTable();
                showSuccessMsg('success_msg', data.msg);
            } else {
                showErrorAlert('save_errors', data.msg);
            }
        }
    });
    //show form
    $("#joggingFormModal").modal('show');
}

/**
 * opens remove expense form
 * @param id - id
 */
function showRemoveForm(id){
    $('#recordId').val(id);
    $('#delete_errors').html('');
    $('#removeModal').modal('show');
}

/**
 * removes expense by id
 */
function removeJogging(){
    var recordId = $('#recordId').val();
    $.ajax({
        type: "delete",
        url : 'jogging/' + recordId,
        success : function(data) {
            if (data.success == 'true'){
                showSuccessMsg('success_msg', data.msg);
                tableData.fnClearTable();
                $('#removeModal').modal('hide');
            } else {
                showErrorAlert('delete_errors', data.msg);
            }
        }
    });
}

function getCurrentDate(){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
    if(dd<10) {
        dd='0'+dd
    }
    if(mm<10) {
        mm='0'+mm
    }
    return mm + '-' + dd + '-' +yyyy;
}

function refreshTableData(){
    tableData.fnClearTable();
}
