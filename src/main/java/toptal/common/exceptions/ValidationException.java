package toptal.common.exceptions;

/**
 * form data validation exception
 */
public class ValidationException extends RuntimeException {
    public ValidationException(String message) {
        super(message);
    }
}
