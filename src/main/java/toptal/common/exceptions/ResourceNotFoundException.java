package toptal.common.exceptions;

/**
 * User: Maksym Goroshkevych Date: 28.05.14 Time: 21:48
 */
public class ResourceNotFoundException extends RuntimeException
{
	public ResourceNotFoundException(String message) {
		super(message);
	}
}
