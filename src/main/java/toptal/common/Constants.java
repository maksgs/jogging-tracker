package toptal.common;

import java.time.format.DateTimeFormatter;
import java.util.Locale;

/**
 * User: Maksym Goroshkevych Date: 28.05.14 Time: 21:36
 */
public class Constants
{
	private Constants()
	{
	}

	public static final String MESSAGE = "message";
	public static final String ERROR = "error";
	public static final String MSG = "msg";
	public static final String SUCCESS = "success";
	public static final String REGISTER_TAB = "registerTab";
	public static final String REDIRECT_TO_LOGIN_PAGE = "redirect:/login.html";
	public static final String REDIRECT_TO_HOME_PAGE = "redirect:/";
	public static final String RECORD = "record";

	public static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm:ss").withLocale(Locale.US);
	public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("MM-dd-yyyy").withLocale(Locale.US);

}
