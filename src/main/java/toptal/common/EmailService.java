package toptal.common;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.inject.Inject;
import javax.mail.internet.InternetAddress;
import java.util.Locale;

/**
 * Service that sends emails
 * Author: Maksym Goroshkevych
 */

@Service
public class EmailService
{
    @Value("${email.username}")
    private String SERVER_EMAIL;

    private static final String SUBJECT_PATTERN = "%s.subject";
    private static final String ISO_ENCODING = "ISO-8859-1";

    @Inject
    private MessageSource messageSource;

    @Inject
    private JavaMailSender mailSender;

    /**
     * sends emails without using templates
     * @param toEmail -to email
     * @param body - email body
     * @param subject - subject
     */
    private void sendEmail( String toEmail, String body, String subject )
    {
        sendEmail(SERVER_EMAIL, toEmail, subject, body, null);
    }

    /**
     * send emails
     * @param toEmail - to email address string
     * @param emailTemplate - email template name
     * @param locale - locale
     * @param bodyParameters - template params
     */
    @Async("mailAsyncTaskExecutor")
    public void sendEmail(String toEmail, String emailTemplate, Locale locale, Object[] bodyParameters){
        String body = messageSource.getMessage(emailTemplate, bodyParameters, locale);
        sendEmail(toEmail, body, getTemplateSubject(emailTemplate, locale));
    }

    /**
     * get templates subject = template name + '.subject'
     * @param emailTemplate
     *      email template name
     * @return - subject string
     */
    private String getTemplateSubject(String emailTemplate, Locale locale){
        String subjectCode = String.format(SUBJECT_PATTERN, emailTemplate);
        return messageSource.getMessage(subjectCode, null, locale);
    }

    /**
     * universal method for sending emails
     * @param fromEmail
     *          from email address
     * @param toEmail
     *          to email address
     * @param subject
     *          email subject
     * @param body
     *          email body
     * @param attachment
     *          file attachment
     */
    private void sendEmail(final String fromEmail, final String toEmail, final String subject, final String body, final CommonsMultipartFile attachment){
        mailSender.send(mimeMessage -> {
			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true, ISO_ENCODING);
			messageHelper.setFrom(  new InternetAddress(fromEmail) );
			messageHelper.setTo( new InternetAddress(toEmail));
			messageHelper.setSubject(subject);
			messageHelper.setText( body, true );
			if (attachment != null){
				// determines if there is an upload file, attach it to the e-mail
				String attachName = attachment.getOriginalFilename();
					if (!attachName.equals("")) {
						messageHelper.addAttachment(attachName, attachment::getInputStream);
					}
				}
			}
		);
    }

}
