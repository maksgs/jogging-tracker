package toptal.common;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import toptal.common.utils.ReportUtils;
import toptal.model.tracker.Jogging;

import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

/**
 * User: Maksym Goroshkevych Date: 22.03.14 Time: 13:33
 * converts objects to jquery datatable json
 */
@Component
public class TableHelper
{
	private static final String DISTANCE_FORMAT = "%d m";
	private static final String SPEED_FORMAT = "%s km/hour";

	private final static ObjectMapper MAPPER = new ObjectMapper();

	public String convert(Page<Jogging> page) throws IOException
	{
		TableResult tableResult = new TableResult();
		tableResult.setiTotalDisplayRecords(Long.toString(page.getTotalElements()));
		tableResult.setiTotalRecords(Long.toString(page.getTotalElements()));
		for(Jogging item: page.getContent()){
			List<String> rowData = new ArrayList<>();
			rowData.add(item.getDate());
			rowData.add(LocalTime.ofSecondOfDay(item.getTime()).format(Constants.TIME_FORMATTER));
			rowData.add(String.format(DISTANCE_FORMAT, item.getDistance()));
			rowData.add(String.format(SPEED_FORMAT,
					ReportUtils.getSpeed(item.getDistance(), item.getTime()).toString()));
			rowData.add(item.getComment());
			//action columns
			rowData.add(item.getId().toString());
			rowData.add(item.getDistance().toString());
			tableResult.getAaData().add(rowData);
		}
		return MAPPER.writeValueAsString(tableResult);
	}


	private static class TableResult{
		//total results on the page
		private String iTotalDisplayRecords;
		//total items
		private String iTotalRecords;
		//table data
		private List<List<String>> aaData = new ArrayList<>();

		public List<List<String>> getAaData()
		{
			return aaData;
		}

		public void setAaData(List<List<String>> aaData)
		{
			this.aaData = aaData;
		}

		public String getiTotalDisplayRecords()
		{
			return iTotalDisplayRecords;
		}

		public void setiTotalDisplayRecords(String iTotalDisplayRecords)
		{
			this.iTotalDisplayRecords = iTotalDisplayRecords;
		}

		public String getiTotalRecords()
		{
			return iTotalRecords;
		}

		public void setiTotalRecords(String iTotalRecords)
		{
			this.iTotalRecords = iTotalRecords;
		}
	}
}
