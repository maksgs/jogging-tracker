package toptal.common.utils;

import com.google.common.base.Strings;
import org.springframework.data.domain.Sort;

/**
 * User: Maksym Goroshkevych Date: 30.05.14 Time: 22:20
 */
public class PageUtils
{
	private PageUtils(){};
	/**
	 * get page number by offset and number of items on page
	 * @param start - offset
	 * @param size - items per page
	 * @return page number
	 */
	public static int getPage(int start, int size){
		if (start == 0 || start < size){
			return 0;
		}
		return (start / size);
	}

	/**
	 * returns sort direction
	 * @param sortOrder - sort order in string
	 * @return {@link org.springframework.data.domain.Sort.Direction}
	 */
	public static Sort.Direction getSortOrder(String sortOrder)
	{
		if (Strings.isNullOrEmpty(sortOrder)){
			return Sort.Direction.ASC;
		}
		switch (sortOrder){
			case "desc":
				return Sort.Direction.DESC;
			default:
				return Sort.Direction.ASC;
		}
	}
}
