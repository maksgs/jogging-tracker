package toptal.common.utils;

import toptal.common.Constants;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

/**
 * User: Maksym Goroshkevych Date: 31.05.14 Time: 9:55
 */
public class DateUtils
{

	private DateUtils(){}

	private static final Date DEFAULT_FROM_DATE = convertStringToDate("01-01-0001");
	private static final Date DEFAULT_TO_DATE = convertStringToDate("01-01-9999");

	private static final String DATE_RANGE_SPLITTER = "~";

	/**
	 * converts mm-dd-yyyy to java.util.Date
	 * @param date source
	 * @return {@link Date}
	 */
	public static Date convertStringToDate (String date){
		return convertLocalDateToDate(convertStringToLocalDate(date));
	}

	/**
	* converts mm-dd-yyyy to java.time.LocalDate
	 * @param date source
	 * @return {@link LocalDate}
	 */
	public static LocalDate convertStringToLocalDate (String date){
		return LocalDate.parse(date, Constants.DATE_FORMATTER);
	}

	/**
	 * converts java.time.LocalDate to java.util.Date
	 * @param ld source
	 * @return {@link Date}
	 */
	public static Date convertLocalDateToDate (LocalDate ld){
		return Date.from(ld.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
	}

	/**
	 * retrieves from date from datatable filter
	 * @param dateRange date range in format (mm-dd-yyyy~mm-dd-yyyy)
	 * @return {@link Date}
	 */
	public static Date getFromDate(String dateRange) {
		if (dateRange == null || dateRange.isEmpty() || !dateRange.contains(DATE_RANGE_SPLITTER)){
			return DEFAULT_FROM_DATE;
		}
		try{
			String s = dateRange.split(DATE_RANGE_SPLITTER)[0];
			return convertStringToDate(s);
		}catch (Exception ex ){
			return DEFAULT_FROM_DATE;
		}
	}

	/**
	 * retrieves to date from datatable filter
	 * @param dateRange date range in format (mm-dd-yyyy~mm-dd-yyyy)
	 * @return {@link Date}
	 */
	public static Date getToDate(String dateRange) {
		if (dateRange == null || dateRange.isEmpty() || !dateRange.contains(DATE_RANGE_SPLITTER)){
			return DEFAULT_TO_DATE;
		}
		try{
			return convertStringToDate(dateRange.split(DATE_RANGE_SPLITTER)[1]);
		}catch (Exception ex ){
			return DEFAULT_TO_DATE;
		}
	}
}
