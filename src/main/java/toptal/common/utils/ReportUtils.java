package toptal.common.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * User: Maksym Goroshkevych Date: 24.06.14 Time: 17:26
 */
public class ReportUtils
{
	private ReportUtils(){

	}

	/**
	 * calculates speed
	 * @param distance - distance
	 * @param seconds - time in seconds
	 * @return average speed
	 */
	public static Double getSpeed(Integer distance, Integer seconds){
		Double result = ( distance * 3.6 ) / seconds;
		BigDecimal bd = new BigDecimal(result).setScale(2, RoundingMode.HALF_EVEN);
		return bd.doubleValue();
	}
}
