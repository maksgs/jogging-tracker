package toptal.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;
import toptal.model.user.User;

@Transactional
public interface UserRepository extends PagingAndSortingRepository<User, Long> {

    User findByEmail(String email);

	User findByConfirmationToken(String token);

}
