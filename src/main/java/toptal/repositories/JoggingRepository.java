package toptal.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import toptal.model.tracker.Jogging;
import toptal.model.tracker.JoggingReportItem;

import java.util.Date;
import java.util.List;

/**
 * User: Maksym Goroshkevych
 * Date: 28.05.14 Time: 21:05
 */
@Transactional
public interface JoggingRepository extends PagingAndSortingRepository<Jogging, Long>
{
	String REPORT_DATA_QUERY = "select "
			+ " EXTRACT(year from to_date(date,  'MM-DD-YYYY')) as year, "
			+ " EXTRACT(week from to_date(date,  'MM-DD-YYYY')) as week, "
			+ " sum(time) as totalTime,"
			+ " sum(distance) as totalDistance,"
			+ " count(id) as times"
			+ " from jogging where fk_user_id = :userId"
			+ " group by "
			+ " EXTRACT(year from to_date(date,  'MM-DD-YYYY')), "
			+ " EXTRACT(week from to_date(date,  'MM-DD-YYYY'))"
			+ " order by year desc,week desc";

	Jogging findByIdAndUserId(Long itemId, Long userId);

	@Query("SELECT j FROM Jogging j WHERE j.user.id = :userId and to_date(j.date, 'MM-DD-YYYY') between :from  and :to")
	Page<Jogging> findUserJogging(@Param("userId") Long userId, @Param("from") Date from,
								  @Param("to") Date to, Pageable pageable);

	@Query(nativeQuery = true, value = REPORT_DATA_QUERY)
	List<Object[]> getReportData(@Param("userId") Long userId);
}
