package toptal.tracker.services;

import com.google.common.collect.ImmutableMap;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import toptal.common.Constants;
import toptal.common.utils.DateUtils;
import toptal.model.tracker.Jogging;
import toptal.model.tracker.JoggingReportItem;
import toptal.model.tracker.JoggingVO;
import toptal.model.user.User;
import toptal.repositories.JoggingRepository;

import javax.inject.Inject;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * User: Maksym Goroshkevych Date: 24.06.14 Time: 17:05
 */
@Service
public class JoggingService
{

	@Inject
	private JoggingRepository repository;

	@Inject
	private JoggingMapper mapper;

	@Inject
	private MessageSource messageSource;

	/**
	 * returns user expenses
	 * @param user - user
	 * @param page - page number
	 * @param pageSize - items on page
	 * @param sortColumn - sort column name
	 * @param sortOrder - sort order
	 * @return page of expenses data
	 */
	public Page<Jogging> getUserJogging(User user, int page, int pageSize,
										 String sortColumn, Sort.Direction sortOrder, String dateRange){
		Pageable pageable = new PageRequest(page, pageSize, new Sort(sortOrder, sortColumn ));
		Date fromDate = DateUtils.getFromDate(dateRange);
		Date toDate = DateUtils.getToDate(dateRange);
		return repository.findUserJogging(user.getId(), fromDate, toDate, pageable);
	}

	/**
	 * returns jogging items by user id and id
	 * @param loggedUser - logged user
	 * @param itemId - jogging id
	 * @return Jogging or null
	 */
	public Jogging getJogging(User loggedUser, Long itemId)
	{
		return repository.findByIdAndUserId(itemId, loggedUser.getId() );
	}

	/**
	 * removes jogging
	 * @param loggedUser - logged user
	 * @param joggingId - jogging id
	 * @param locale - locale
	 */
	@Transactional
	public Map<String, String> deleteJogging(User loggedUser, Long joggingId, Locale locale)
	{
		Jogging jogging = repository.findByIdAndUserId(joggingId, loggedUser.getId());
		if (jogging == null){
			return ImmutableMap.of(Constants.SUCCESS, "false",
					Constants.MSG, messageSource.getMessage("record.was_not_found.error_msg", null, locale));
		}
		repository.delete(jogging);
		return  ImmutableMap.of(Constants.SUCCESS, "true",
				Constants.MSG, messageSource.getMessage("record_was_removed.msg", null, locale));
	}

	/**
	 * saves jogging data
	 * @param loggedUser - loggedUser
	 * @param joggingVO - jogging data
	 * @param locale - locale
	 * @return status of save operation
	 */
	public Map<String, String> saveJogging(User loggedUser, JoggingVO joggingVO, Locale locale)
	{
		joggingVO.setId(null);
		Jogging jogging = mapper.map(joggingVO);
		jogging.setUser(loggedUser);
		repository.save(jogging);
		return ImmutableMap.of(Constants.SUCCESS, "true",
				Constants.MSG, messageSource.getMessage("record_was_saved.msg", null, locale));
	}

	/**
	 * saves edited jogging data
	 * @param loggedUser - loggedUser
	 * @param jogging - jogging data
	 * @param locale - locale
	 * @return status of save operation
	 */
	@Transactional
	public Map<String, String> editJogging(User loggedUser, JoggingVO jogging, Locale locale)
	{
		Jogging oldData = repository.findByIdAndUserId(jogging.getId(), loggedUser.getId() );
		if (oldData == null){
			return ImmutableMap.of(Constants.SUCCESS, "false",
					Constants.MSG, messageSource.getMessage("record.was_not_found.error_msg", null, locale));
		}
		Jogging newData = mapper.map(jogging);
		oldData.setDate(newData.getDate());
		oldData.setTime(newData.getTime());
		oldData.setDistance(newData.getDistance());
		oldData.setComment(newData.getComment());
		repository.save(oldData);
		return ImmutableMap.of(Constants.SUCCESS, "true", Constants.MSG,
				messageSource.getMessage("record_was_saved.msg", null, locale));
	}

	/**
	 * retrieves all required data for report
	 * @return {@link List<JoggingReportItem>}
	 */
	public List<JoggingReportItem> getTotalReportData(Long userId) {
		List<Object[]> dbData = repository.getReportData(userId);
		return dbData.stream().map(mapper::map).collect(Collectors.toList());
	}
}
