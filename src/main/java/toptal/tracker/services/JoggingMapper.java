package toptal.tracker.services;

import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import toptal.common.Constants;
import toptal.common.utils.ReportUtils;
import toptal.model.tracker.Jogging;
import toptal.model.tracker.JoggingReportItem;
import toptal.model.tracker.JoggingVO;

import java.math.BigInteger;
import java.time.LocalTime;

/**
 * User: Maksym Goroshkevych
 * Date: 03.07.14
 * Time: 22:55
 */
@Component
public class JoggingMapper {

	/**
	 * maps jogging from view object to hibernate entity
	 * @param vo jogging view object
	 * @return {@link Jogging}
	 */
	public Jogging map(JoggingVO vo){
		Jogging result = new Jogging();
		result.setId(vo.getId());
		result.setTime(LocalTime.parse(vo.getTime(), Constants.TIME_FORMATTER).toSecondOfDay());
		result.setDate(vo.getDate());
		result.setDistance(vo.getDistance());
		result.setComment(vo.getComment());
		return result;
	}

	/**
	 * maps params VO
	 * @param params joggingVO in map
	 * @return {@link JoggingVO}
	 */
	public JoggingVO map(MultiValueMap<String, String> params){
		JoggingVO result = new JoggingVO();
		result.setTime(params.getFirst("time") == null ? null: params.getFirst("time"));
		result.setDate(params.getFirst("date") == null ? null: params.getFirst("date"));
		result.setDistance(params.getFirst("distance") == null ? 0: new Integer(params.getFirst("distance")));
		result.setComment(params.getFirst("comment") == null ? null: params.getFirst("comment"));
		return result;
	}

	public JoggingReportItem map(Object[] source){
		JoggingReportItem item = new JoggingReportItem();
		//map db value
		item.setYear(((Double) source[0]).intValue());
		item.setWeek(((Double) source[1]).intValue());
		item.setTotalTime(((BigInteger) source[2]).longValue());
		item.setTotalDistance(((BigInteger) source[3]).longValue());
		item.setTimes(((BigInteger) source[4]).longValue());
		//calculate statistic
		item.setAverageDistance(item.getTotalDistance() / item.getTimes());
		item.setAverageTime(item.getTotalTime() / item.getTimes());
		item.setAverageSpeed(ReportUtils.getSpeed(item.getTotalDistance().intValue(), item.getTotalTime().intValue()));
		return item;
	}
}
