package toptal.tracker.helpers;

import org.springframework.stereotype.Component;

/**
 * User: Maksym Goroshkevych Date: 24.06.14 Time: 17:24
 */
@Component
public class JoggingHelper
{
	private static final String SORT_COLUMN_0 = "date";
	private static final String SORT_COLUMN_1 = "time";
	private static final String SORT_COLUMN_2 = "distance";
	private static final String SORT_COLUMN_3 = "comment";
	private static final String SORT_COLUMN_DEFAULT = "id";

	public String getSortColumn(int sortBy)
	{
		switch (sortBy){
			case 0:
				return SORT_COLUMN_0;
			case 1:
				return SORT_COLUMN_1;
			case 2:
				return SORT_COLUMN_2;
			case 4:
				return SORT_COLUMN_3;
			default:
				return SORT_COLUMN_DEFAULT;
		}
	}
}
