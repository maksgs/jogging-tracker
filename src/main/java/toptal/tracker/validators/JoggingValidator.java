package toptal.tracker.validators;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import toptal.common.Constants;
import toptal.model.tracker.JoggingVO;

import javax.inject.Inject;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.Locale;

/**
 * Validates expense input data
 * User: Maksym Goroshkevych Date: 30.05.14 Time: 23:59
 */
@Component
public class JoggingValidator {

	private static final int WRONG_TIME_FORMAT_LENGTH = 7;
	private static final LocalTime zeroTime = LocalTime.parse("00:00:00", Constants.TIME_FORMATTER);

	@Inject
	private MessageSource messageSource;

	public String validate(JoggingVO source, Locale locale) {
		if (source.getDate() == null || source.getDate().isEmpty()) {
			return messageSource.getMessage("date_could_not_be_null.error.msg", null, locale);
		}
		if (source.getTime() == null || source.getTime().isEmpty()) {
			return messageSource.getMessage("time_could_not_be_null.error.msg", null, locale);
		}
		if (source.getDistance() == null || source.getDistance() <= 0) {
			return messageSource.getMessage("distance_could_not_be_null.error.msg", null, locale);
		}
		try {
			if (source.getTime().length() == WRONG_TIME_FORMAT_LENGTH){
				source.setTime(String.format("0%s", source.getTime()));
			}
			LocalTime time = LocalTime.parse(source.getTime(), Constants.TIME_FORMATTER);
			if (!time.isAfter(zeroTime)){
				return messageSource.getMessage("time_is_not_valid.error.msg", null, locale);
			}
		}catch (DateTimeParseException ex){
			return messageSource.getMessage("time_is_not_valid.error.msg", null, locale);
		}
		try{
			LocalDate.parse(source.getDate(), Constants.DATE_FORMATTER);
		}catch (DateTimeParseException ex){
			return messageSource.getMessage("date_is_not_valid.error.msg", null, locale);
		}
		return null;
	}
}
