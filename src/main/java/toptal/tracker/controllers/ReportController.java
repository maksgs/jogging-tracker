package toptal.tracker.controllers;

import com.itextpdf.text.DocumentException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import toptal.model.tracker.JoggingReportItem;
import toptal.model.user.User;
import toptal.tracker.helpers.ReportHelper;
import toptal.tracker.services.JoggingService;
import toptal.user.services.LoggedUserService;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * User: Maksym Goroshkevych Date: 24.06.14 Time: 16:57
 */
@Controller
@RequestMapping("/reports")
public class ReportController
{
	@Inject
	private ReportHelper reportHelper;

	@Inject
	private LoggedUserService userService;

	@Inject
	private JoggingService joggingService;

	@RequestMapping
	public void getDashboardPage(HttpServletResponse response, Locale locale) throws IOException, DocumentException {
		User user = userService.getUser();
		List<JoggingReportItem> data = joggingService.getTotalReportData(user.getId());
		reportHelper.generateReport(response, user, locale, data);
	}
}
