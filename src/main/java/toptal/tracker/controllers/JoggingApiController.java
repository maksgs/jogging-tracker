package toptal.tracker.controllers;

import com.google.common.base.Strings;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import toptal.common.Constants;
import toptal.common.TableHelper;
import toptal.common.utils.PageUtils;
import toptal.model.tracker.Jogging;
import toptal.model.tracker.JoggingVO;
import toptal.model.user.User;
import toptal.tracker.helpers.JoggingHelper;
import toptal.tracker.services.JoggingMapper;
import toptal.tracker.services.JoggingService;
import toptal.tracker.validators.JoggingValidator;
import toptal.user.services.LoggedUserService;

import javax.inject.Inject;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * User: Maksym Goroshkevych
 * Date: 28.05.14 Time: 21:03
 */
@RestController
@RequestMapping(value = {"/api/jogging", "/jogging"}, produces = MediaType.APPLICATION_JSON_VALUE)
public class JoggingApiController {
	@Inject
	private JoggingService joggingService;

	@Inject
	private LoggedUserService userService;

	@Inject
	private JoggingHelper joggingHelper;

	@Inject
	private JoggingMapper mapper;

	@Inject
	private TableHelper tableHelper;

	@Inject
	private JoggingValidator validator;

	/**
	 * returns jogging list according to page attributes
	 * @param start - number of first element
	 * @param dateRange - date range in string format from~to
	 * @param sortBy - number of sorted column
	 * @param sortOrder - sort order
	 * @param size - number of items on page
	 * @return compressed json data
	 * @throws java.io.IOException
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public String getJoggingList(@RequestParam(value = "iDisplayStart", defaultValue = "0", required = false) int start,
							  @RequestParam(value = "sSearch_5", defaultValue = "", required = false) String dateRange,
							  @RequestParam(value = "iSortCol_0", defaultValue = "0", required = false) int sortBy,
							  @RequestParam(value = "sSortDir_0", defaultValue = "asc", required = false) String sortOrder,
							  @RequestParam(value = "iDisplayLength",defaultValue = "10", required = false) int size) throws IOException{
		User loggedUser = userService.getUser();
		Page<Jogging> result = joggingService.getUserJogging(loggedUser, PageUtils.getPage(start, size), size,
				joggingHelper.getSortColumn(sortBy), PageUtils.getSortOrder(sortOrder), dateRange);
		return tableHelper.convert(result);
	}

	/**
	 * returns jogging list according to page attributes
	 * @param start - number of first element
	 * @param dateRange - date range in string format from~to
	 * @param sortBy - number of sorted column
	 * @param sortOrder - sort order
	 * @param size - number of items on page
	 * @return list in json
	 */
	@RequestMapping
	@ResponseBody
	public List<Jogging> getJoggingJsonList(@RequestParam(value = "iDisplayStart", defaultValue = "0", required = false) int start,
											@RequestParam(value = "sSearch_5", defaultValue = "", required = false) String dateRange,
											@RequestParam(value = "iSortCol_0", defaultValue = "0", required = false) int sortBy,
											@RequestParam(value = "sSortDir_0", defaultValue = "asc", required = false) String sortOrder,
											@RequestParam(value = "iDisplayLength",defaultValue = "10", required = false) int size){
		User loggedUser = userService.getUser();
		Page<Jogging> result = joggingService.getUserJogging(loggedUser, PageUtils.getPage(start, size), size,
				joggingHelper.getSortColumn(sortBy), PageUtils.getSortOrder(sortOrder), dateRange);
		return result.getContent();
	}

	/**
	 * return jogging by id
	 * @param itemId - jogging id
	 * @return {@link Jogging}
	 */
	@RequestMapping("/{itemId}")
	@ResponseBody
	public Jogging getJogging(@PathVariable Long itemId){
		User loggedUser = userService.getUser();
		return joggingService.getJogging(loggedUser, itemId);

	}

	/**
	 * saves jogging
	 * @param jogging - jogging
	 * @param locale - locale
	 * @return error message or success msg if jogging was saved
	 */
	@RequestMapping(method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> saveJogging(@ModelAttribute JoggingVO jogging, Locale locale) {
		Map<String, Object> result = new HashMap<>();
		User loggedUser = userService.getUser();
		String validationErrorMsg = validator.validate(jogging, locale);
		if (!Strings.isNullOrEmpty(validationErrorMsg)){
			result.put(Constants.SUCCESS, false);
			result.put( Constants.MSG, validationErrorMsg);
			return result;
		}
		result.putAll(joggingService.saveJogging(loggedUser, jogging, locale));
		return result;

	}

	/**
	 * edit jogging data
	 * @param joggingId - jogging id
	 * @param locale - locale
	 * @return error message or success msg if expense was saved
	 */
	@RequestMapping(method = RequestMethod.PUT, value = "/{joggingId}")
	@ResponseBody
	public Map<String, Object> editJogging(@PathVariable Long joggingId, @RequestParam MultiValueMap<String, String> params, Locale locale) {
		User loggedUser = userService.getUser();
		JoggingVO jogging = mapper.map(params);
		Map<String, Object> result = new HashMap<>();
		String validationErrorMsg = validator.validate(jogging, locale);
		if (!Strings.isNullOrEmpty(validationErrorMsg)){
			result.put(Constants.SUCCESS, false);
			result.put( Constants.MSG, validationErrorMsg);
			return result;
		}
		jogging.setId(joggingId);
		result.putAll(joggingService.editJogging(loggedUser, jogging, locale));
		return result;

	}

	/**
	 * removes jogging item
	 * @param joggingId - jogging id
	 * @param locale - locale
	 * @return returns message about the status of remove
	 */
	@RequestMapping(value = "/{joggingId}", method = RequestMethod.DELETE)
	@ResponseBody
	public Map<String, Object> delete(@PathVariable Long joggingId, Locale locale) {
		Map<String, Object> result = new HashMap<>();
		User loggedUser = userService.getUser();
		result.putAll(joggingService.deleteJogging(loggedUser, joggingId, locale));
		return result;

	}

}
