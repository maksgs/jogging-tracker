package toptal.tracker.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import toptal.common.Constants;
import toptal.model.tracker.Jogging;

/**
 * User: Maksym Goroshkevych Date: 28.05.14 Time: 21:51
 */
@Controller
@RequestMapping("/")
public class DashboardController
{

	private static final String DASHBOARD_TEMPLATE = "dashboard/dashboard";

	@RequestMapping
	public String getDashboardPage(Model model){
		model.addAttribute(Constants.RECORD, new Jogging());
		return DASHBOARD_TEMPLATE;
	}

}
