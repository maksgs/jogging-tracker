package toptal.user;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import toptal.common.exceptions.ValidationException;

import javax.inject.Inject;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.util.Locale;

/**
 * validates user data
 */
@Component
public class UserValidator
{

    private static final Integer PASSWORD_MIN_LENGTH = 6;

    @Inject
    private MessageSource messageSource;

    public void validate(String email, String password, Locale locale){
        validatePassword(password, locale);
        validateEmail(email, locale);

    }

    /**
     * throws exception if email is not correct
     * @param email - email
     * @param locale - user locale
     */
    public void validateEmail(String email, Locale locale){
        if (email == null || email.isEmpty()){
            throw new ValidationException(messageSource.getMessage("connection_email_null.error.msg", null, locale));
        }
        if (!isValidEmailAddress(email)){
            throw new ValidationException(messageSource.getMessage("wrong_email_format.error.msg", null ,locale));
        }
    }

    /**
     * returns if email format is valid
     * @param email - email
     * @return - boolean
     */
    private static boolean isValidEmailAddress(String email) {
        boolean result = true;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            result = false;
        }
        return result;
    }


    public void validatePassword(String password, Locale locale) {
        if (password == null || password.isEmpty()){
            throw new ValidationException(messageSource.getMessage("connection_password_null.error.msg", null ,locale));
        }
        if (password.length() < PASSWORD_MIN_LENGTH){
            throw new ValidationException(messageSource.getMessage("password_to_low.error.msg", null, locale));
        }
    }
}
