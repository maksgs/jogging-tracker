package toptal.user.services;

import com.google.common.collect.Lists;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import toptal.common.EmailService;
import toptal.model.user.User;
import toptal.repositories.UserRepository;

import javax.inject.Inject;
import javax.servlet.ServletContext;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

/**
 * service layer for interacting with user's data
 */
@Service
public class AuthService
{
    private static final String CONFIRMATION_URL = "%s/%s/email/confirmation/%s";

    @Value("${server.url}")
    private String serverUrl;

	@Inject
	private ServletContext context;

    @Inject
    private UserRepository userRepository;

    @Inject
    private EmailService emailService;

	@Inject
    private StandardPasswordEncoder encoder;

	/**
	 * sets encoder
	 * @param encoder - encoder
	 */
	public void setEncoder(StandardPasswordEncoder encoder)
	{
		this.encoder = encoder;
	}

    /**
     * register new user in system
     * @param email - user email
     * @param password - password
     */
    @Transactional
    public void signUp(String email, String password, Locale locale) {
        User user = new User();
		user.setEmail(email);
        user.setPassword(encoder.encode(password));
        user.setRegisterDate(new DateTime());
        user.setEnabled(false);
		user.setConfirmationToken(UUID.randomUUID().toString());
        userRepository.save(user);
        sendConfirmationEmail(user, locale);
    }

    /**
     * sends confirmation url in email
     * @param user - new user entity
     * @param locale - locale
     */
    private void sendConfirmationEmail(User user, Locale locale){
        emailService.sendEmail(user.getEmail(), "confirm_registration.email", locale,
				new Object[]{
						String.format(CONFIRMATION_URL, serverUrl, context.getContextPath(), user.getConfirmationToken())
				});

    }

    /**
     * returns user by confirmation token
     * @param token - confirmation token
     * @return user
	 */
    public User getUserByConfirmationToken(String token) {
        return userRepository.findByConfirmationToken(token);
    }

    /**
     * saves user data
     * @param user - saves user
     */
    public void save(User user) {
        userRepository.save(user);
    }

	/**
	 * returns user by email
	 * @param email - email
	 * @return {@link User}
	 */
	public User findUserByEmail(String email)
	{
		return userRepository.findByEmail(email);
	}

	/**
	 * returns all users in the system
	 * @return - list of users
	 */
	public List<User> getAllUsers()
	{
		return Lists.newArrayList(userRepository.findAll());
	}
}
