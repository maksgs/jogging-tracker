package toptal.user.services;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import toptal.repositories.UserRepository;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;

@Service("authUserDetails")
public class AuthUserDetails implements UserDetailsService {

    @Inject
    private UserRepository userRepository;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		toptal.model.user.User user = userRepository.findByEmail(email);
		if (user == null) {
			throw new UsernameNotFoundException("User is not found");
		}
		return buildUserFromUserEntity(user);
	}

	private User buildUserFromUserEntity(toptal.model.user.User user) {
		String email = user.getEmail();
		String password = user.getPassword();

		boolean enabled = true;
		boolean accountNonExpired = true;
		boolean credentialsNonExpired = true;
		boolean accountNonLocked = user.getEnabled();

		// All authorized user has the role ROLE_USER
		Collection<GrantedAuthority> authorities = new ArrayList<>();
		authorities.add(new SimpleGrantedAuthority(user.getRole().name()));

		return new User(email, password, enabled, accountNonExpired, credentialsNonExpired,
				accountNonLocked, authorities);
	}
}
