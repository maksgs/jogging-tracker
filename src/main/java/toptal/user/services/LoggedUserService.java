package toptal.user.services;


import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;
import toptal.model.user.User;
import toptal.repositories.UserRepository;

import javax.inject.Inject;

/**
 * this service will return user data from session
 */
@Service
@Scope (proxyMode = ScopedProxyMode.TARGET_CLASS, value = WebApplicationContext.SCOPE_SESSION)
public class LoggedUserService
{
    private User loggedUser = null;
    private static final String ANONYMOUS_USER = "anonymousUser";

    @Inject
    private UserRepository userRepository;

    /**
     * returns authenticated user, or null if user is not authenticated
     * @return user
     */
    public User getUser()
    {
		try{
			if (loggedUser == null
					|| !loggedUser.getEmail().equals(SecurityContextHolder.getContext().getAuthentication().getName()) ){
				String email = SecurityContextHolder.getContext().getAuthentication().getName();
				if (ANONYMOUS_USER.equals(email)){
					return null;
				}
				loggedUser = userRepository.findByEmail(email);
			}
        }catch(NullPointerException ex){
        	//user is not authenticated
            return null;
        }
        return loggedUser;
    }

    /**
     * refreshes user data
     */
    public void refreshUser()
    {
        loggedUser = null;
    }
}

