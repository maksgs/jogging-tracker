package toptal.user.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import toptal.common.Constants;

@Controller
public class FaviconController {

	@RequestMapping("favicon.ico")
	public String favicon() {
		return Constants.REDIRECT_TO_HOME_PAGE;
	}
}
