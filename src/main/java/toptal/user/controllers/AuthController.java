package toptal.user.controllers;

import com.google.common.collect.ImmutableMap;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import toptal.common.Constants;
import toptal.common.exceptions.ResourceNotFoundException;
import toptal.common.exceptions.ValidationException;
import toptal.model.user.User;
import toptal.user.UserValidator;
import toptal.user.services.AuthService;
import toptal.user.services.LoggedUserService;

import javax.inject.Inject;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * controller to handle signup, login, profile requests
 */
@Controller
public class AuthController
{

    private static final String LOGIN_TEMPLATE = "login";

    @Inject
    private AuthService authenticationService;

    @Inject
    private MessageSource messageSource;

	@Inject
	private LoggedUserService userService;

    @Inject
    private UserValidator validator;

	/**
	 * creates user, handler for signUp form
	 * @param email - email
	 * @param password - password
	 * @param locale - user locale
	 * @param redirectAttrs - model
	 * @return - redirected page
	 */
    @RequestMapping(value = "/auth/sign-up", method = RequestMethod.POST)
    public String signUp (@RequestParam String email, @RequestParam String password,
                          Locale locale, RedirectAttributes redirectAttrs ) {
        try {
            User user = authenticationService.findUserByEmail(email);
            if (user != null){
                throw new ValidationException(messageSource.getMessage("duplicated_email.error.msg", null, locale));
            }
            validator.validate(email, password, locale);
        }catch (ValidationException ex){
            redirectAttrs.addFlashAttribute(Constants.ERROR, ex.getMessage());
            redirectAttrs.addFlashAttribute(Constants.REGISTER_TAB, true);
            return Constants.REDIRECT_TO_LOGIN_PAGE;
        }
        authenticationService.signUp(email, password, locale);
        redirectAttrs.addFlashAttribute(Constants.MESSAGE, messageSource.getMessage("sign_up_success.msg", null, locale));
        return Constants.REDIRECT_TO_LOGIN_PAGE;
    }

	/**
	 * creates user using api
	 * @param email - email
	 * @param password - password
	 * @param locale - locale
	 * @return status of creation
	 */
	@RequestMapping(value = "/api/auth/sign-up", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, String> signUpApi (@RequestParam String email, @RequestParam String password,
						  Locale locale) {
		User user = authenticationService.findUserByEmail(email);
		if (user != null){
			return ImmutableMap.of(Constants.SUCCESS, Boolean.FALSE.toString(), Constants.MSG,
					messageSource.getMessage("duplicated_email.error.msg", null, locale));
		}
		try{
			validator.validate(email, password, locale);
		}catch (ValidationException ex){
			return ImmutableMap.of(Constants.SUCCESS, Boolean.FALSE.toString(), Constants.MSG, ex.getMessage());
		}
		authenticationService.signUp(email, password, locale);
		return ImmutableMap.of(Constants.SUCCESS, Boolean.TRUE.toString(), Constants.MSG,
				messageSource.getMessage("sign_up_success.msg", null, locale));
	}

	/**
	 * returns list of all users
	 * @return - list of all users
	 */
	@RequestMapping("/api/user/list")
	@ResponseBody
	public List<User> getUsers(){
		return authenticationService.getAllUsers();
	}

	/**
	 * handle login form page
	 * @return login page
	 */
	@RequestMapping("/login")
    public String getLoginPage(){
		User user = userService.getUser();
		if (user != null){

			return Constants.REDIRECT_TO_HOME_PAGE;
		}
        return LOGIN_TEMPLATE;
    }

	/**
	 * handle login error page
	 * @param model - page model
	 * @param locale - user locale
	 * @return redirect to login page
	 */
    @RequestMapping("/login-error")
    public String getLoginErrorPage(RedirectAttributes model, Locale locale){
        model.addFlashAttribute(Constants.ERROR, messageSource.getMessage("bad_credentials", null, locale));
        return Constants.REDIRECT_TO_LOGIN_PAGE;
    }

	/**
	 * url to confirm user email
	 * @param model - page model
	 * @param token - confirmation token
	 * @param locale - user locale
	 * @return login page if confirmation url was correct
	 */
    @RequestMapping("/email/confirmation/{token}")
    public String confirmEmail(RedirectAttributes model, @PathVariable String token, Locale locale){
        User user = authenticationService.getUserByConfirmationToken(token);
        if (user == null){
            throw new ResourceNotFoundException("Page does not exist!");
        } else {
            user.setEnabled(true);
            authenticationService.save(user);
            model.addFlashAttribute(Constants.MESSAGE, messageSource.getMessage("email_confirmed.msg", null, locale));
        }
        return Constants.REDIRECT_TO_LOGIN_PAGE;
    }

}
