package toptal.model.user;

import lombok.Getter;
import lombok.Setter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table (name = "users")
public class User
{
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    @Column(nullable = false)
    private Long id;

    @Getter
    @Setter
    @Column(name = "name")
    private String name;

    @Getter
    @Setter
    @Column(nullable = false)
	@JsonIgnore
    private String password;

    @Getter
    @Setter
    @Column(name="register_date")
    @Type (type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime registerDate;

    @Getter
    @Setter
    @Column(nullable = false, columnDefinition = "VARCHAR(20)")
    @Enumerated(EnumType.STRING)
    private toptal.model.user.UserRole role = UserRole.ROLE_USER;

    @Getter
    @Setter
    @Column(nullable = false)
    private Boolean enabled = false;

    @Getter
    @Setter
    private String email;

	@Column(name = "confirmation_token")
	@Getter
	@Setter
	@JsonIgnore
	private String confirmationToken;

    @Override
    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != this.getClass()){
            return false;
        }
        User user = (User) obj;
        return Objects.equals(user.getId(), id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }
}
