package toptal.model.tracker;

import lombok.Getter;
import lombok.Setter;

/**
 * model for report
 */
public class JoggingReportItem {

	@Getter
	@Setter
	private int year;

	@Getter
	@Setter
	private int week;

	@Getter
	@Setter
	private Long totalDistance;

	@Getter
	@Setter
	private Long totalTime;

	@Getter
	@Setter
	private Long times;

	@Getter
	@Setter
	private Long averageDistance;

	@Getter
	@Setter
	private Long averageTime;

	@Getter
	@Setter
	private Double averageSpeed;
}
