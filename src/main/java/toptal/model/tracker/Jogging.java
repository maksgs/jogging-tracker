package toptal.model.tracker;

import lombok.Getter;
import lombok.Setter;
import org.codehaus.jackson.annotate.JsonIgnore;
import toptal.model.user.User;

import javax.persistence.*;

/**
 * User: Maksym Goroshkevych Date: 24.06.14 Time: 16:46
 */
@Entity
@Table(name = "jogging")
public class Jogging
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter
	@Setter
	@Column(nullable = false)
	private Long id;

	@Getter
	@Setter
	@ManyToOne
	@JoinColumn(name = "fk_user_id", nullable = false)
	@JsonIgnore
	private User user;

	@Getter
	@Setter
	@Column(nullable = false)
	private String date;

	/**
	 * time in seconds
	 */
	@Getter
	@Setter
	@Column(nullable = false)
	private Integer time;

	/**
	 * distance in meters
	 */
	@Getter
	@Setter
	@Column(nullable = false)
	private Integer distance;

	@Getter
	@Setter
	@Column
	private String comment;

}
