package toptal.model.tracker;

import lombok.Getter;
import lombok.Setter;

/**
 * User: Maksym Goroshkevych
 * Date: 03.07.14
 * Time: 22:52
 */
public class JoggingVO {
	@Getter
	@Setter
	private Long id;

	@Getter
	@Setter
	private String date;

	@Getter
	@Setter
	private String time;

	/**
	 * distance in meters
	 */
	@Getter
	@Setter
	private Integer distance;

	@Getter
	@Setter
	private String comment;
}
