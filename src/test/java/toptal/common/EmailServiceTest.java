package toptal.common;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;

import java.util.Locale;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * User: Maksym Goroshkevych Date: 01.06.14 Time: 11:53
 */
@RunWith(MockitoJUnitRunner.class)
public class EmailServiceTest
{
	@Mock
	private MessageSource messageSource;

	@Mock
	private JavaMailSender mailSender;

	@InjectMocks
	private EmailService emailService;

	private static final String EMAIL = "test@test.com";
	private static final String TEMPLATE = "temp";
	private static final Object[] PARAMS = new Object[]{};
	private static final Locale locale = Locale.ENGLISH;
	private static final String MSG = "localizedMessage";

	@Test
	public void sendEmail1(){
		reset(messageSource);
		reset(mailSender);
		when(messageSource.getMessage(TEMPLATE, PARAMS, locale)).thenReturn(MSG);
		emailService.sendEmail(EMAIL, TEMPLATE, locale, PARAMS);
		verify(messageSource, times(1)).getMessage(TEMPLATE, PARAMS, locale);
		verify(mailSender, times(1)).send(any(MimeMessagePreparator.class));
	}

}
