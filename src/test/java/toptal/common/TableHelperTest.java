package toptal.common;

import com.google.common.collect.Lists;
import org.junit.Test;
import org.springframework.data.domain.Page;
import toptal.model.tracker.Jogging;
import toptal.model.user.User;

import java.io.IOException;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * User: Maksym Goroshkevych Date: 31.05.14 Time: 20:14
 */
public class TableHelperTest
{
	private TableHelper helper = new TableHelper();

	@Test
	public void testConvert() throws IOException
	{
		Page<Jogging> page = mock(Page.class);
		Jogging item = new Jogging();
		item.setId(3L);
		item.setUser(new User());
		item.setDate("01-01-2010");
		item.setDistance(3000);
		item.setTime(720);
		item.setComment("comment");
		List<Jogging> expenses = Lists.newArrayList(item);
		when(page.getContent()).thenReturn(expenses);
		when(page.getNumberOfElements()).thenReturn(1);
		when(page.getTotalElements()).thenReturn(1L);
		String result = helper.convert(page);
		assertEquals(result, "{\"iTotalDisplayRecords\":\"1\",\"iTotalRecords\":\"1\",\"aaData\":[[\"01-01-2010\",\"00:12:00\",\"3000 m\",\"15.0 km/hour\",\"comment\",\"3\",\"3000\"]]}");
	}
}
