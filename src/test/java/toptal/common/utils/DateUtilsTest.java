package toptal.common.utils;

import org.junit.Test;
import toptal.common.Constants;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import static junit.framework.TestCase.assertEquals;

public class DateUtilsTest {

	private static final String DATE_STRING = "01-01-2010";
	private static final LocalDate DATE_LOCAL_DATE = LocalDate.parse(DATE_STRING,	Constants.DATE_FORMATTER);
	private static final Date DATE = Date.from(DATE_LOCAL_DATE.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());

	@Test
	public void testConvertStringToDate(){
		assertEquals(DateUtils.convertStringToDate("01-01-2010"), DATE);
	}

	@Test
	public void testConvertLocalDateToDate(){
		assertEquals(DateUtils.convertLocalDateToDate(LocalDate.parse(DATE_STRING, Constants.DATE_FORMATTER)), DATE);
	}

	@Test
	public void testConvertStringToLocalDate(){
		assertEquals(DateUtils.convertStringToLocalDate(DATE_STRING), DATE_LOCAL_DATE);
	}

	@Test
	public void testGetFromDate(){
		assertEquals(DateUtils.getFromDate("01-01-2010~01-01-2011"),
				DateUtils.convertStringToDate("01-01-2010"));
	}

	@Test
	public void testGetToDate(){
		assertEquals(DateUtils.getToDate("01-01-2010~01-01-2011"),
				DateUtils.convertStringToDate("01-01-2011"));
	}
}
