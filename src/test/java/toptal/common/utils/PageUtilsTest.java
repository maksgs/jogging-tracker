package toptal.common.utils;

import org.junit.Test;
import org.springframework.data.domain.Sort;

import static junit.framework.TestCase.assertEquals;

/**
 * User: Maksym Goroshkevych Date: 31.05.14 Time: 14:53
 */
public class PageUtilsTest
{
	@Test
	public void testGetPage(){
		assertEquals(PageUtils.getPage(0, 20), 0);
		assertEquals(PageUtils.getPage(20, 20), 1);
		assertEquals(PageUtils.getPage(19, 20), 0);
		assertEquals(PageUtils.getPage(40, 20), 2);
		assertEquals(PageUtils.getPage(39, 20), 1);
		assertEquals(PageUtils.getPage(86, 50), 1);
	}

	@Test
	public void testGetSortOrder(){
		assertEquals(PageUtils.getSortOrder("asc"), Sort.Direction.ASC);
		assertEquals(PageUtils.getSortOrder("desc"), Sort.Direction.DESC);
		assertEquals(PageUtils.getSortOrder(null), Sort.Direction.ASC);
	}
}
