package toptal.user;

import org.junit.Test;
import toptal.common.Constants;
import toptal.user.controllers.FaviconController;

import static junit.framework.TestCase.assertEquals;

public class FaviconControllerTest {

	private FaviconController controller = new FaviconController();

	@Test
	public void favicon(){
		assertEquals(controller.favicon(), Constants.REDIRECT_TO_HOME_PAGE);
	}
}
