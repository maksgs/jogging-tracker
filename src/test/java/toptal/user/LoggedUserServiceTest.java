package toptal.user;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import toptal.repositories.UserRepository;
import toptal.user.services.LoggedUserService;

import static junit.framework.TestCase.assertNull;

/**
 * User: Maksym Goroshkevych Date: 31.05.14 Time: 20:21
 */
@RunWith(MockitoJUnitRunner.class)
public class LoggedUserServiceTest
{
	@Mock
	private UserRepository userRepository;

	@InjectMocks
	private LoggedUserService userService;

	@Test
	public void testGetUser_notAuthenticated(){
		assertNull(userService.getUser());
	}

	@Test
	public void testRefreshUser(){
		userService.refreshUser();
	}
}
