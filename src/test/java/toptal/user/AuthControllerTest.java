package toptal.user;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import toptal.common.Constants;
import toptal.common.exceptions.ResourceNotFoundException;
import toptal.model.user.User;
import toptal.user.controllers.AuthController;
import toptal.user.services.AuthService;
import toptal.user.services.LoggedUserService;

import java.util.Locale;
import java.util.Map;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.*;

/**
 * User: Maksym Goroshkevych Date: 01.06.14 Time: 12:36
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthControllerTest
{
	@Mock
	private AuthService authenticationService;

	@Mock
	private MessageSource messageSource;

	@Mock
	private LoggedUserService userService;

	@Mock
	private UserValidator validator;

	@InjectMocks
	private AuthController controller;

	@After
	public void cleanUp(){
		reset(userService);
		reset(messageSource);

	}

	@Test
	public void testGetLoginPage_redirect(){
		when(userService.getUser()).thenReturn(new User());
		String result = controller.getLoginPage();
		assertEquals(result, "redirect:/");
	}

	@Test
	public void testGetLoginPage(){
		String result = controller.getLoginPage();
		assertEquals(result, "login");
	}

	@Test
	public void testGetLoginErrorPage(){
		Locale locale = Locale.GERMAN;
		RedirectAttributes model = mock(RedirectAttributes.class);
		when(messageSource.getMessage("bad_credentials", null, locale)).thenReturn("msg");
		String result = controller.getLoginErrorPage(model, locale);
		assertEquals(result, Constants.REDIRECT_TO_LOGIN_PAGE);
		verify(messageSource,times(1)).getMessage("bad_credentials", null, locale);
		verify(model, times(1)).addFlashAttribute(Constants.ERROR, "msg");
	}

	@Test
	public void testSignUp_alreadyExist(){
		String email = "test@test.com";
		String password = "password";
		Locale locale = Locale.GERMAN;
		User user = new User();
		RedirectAttributes model = mock(RedirectAttributes.class);
		when(authenticationService.findUserByEmail(email)).thenReturn(user);
		when(messageSource.getMessage("duplicated_email.error.msg", null, locale)).thenReturn("msg");
		String result = controller.signUp(email, password, locale, model);
		assertEquals(result, Constants.REDIRECT_TO_LOGIN_PAGE);
		verify(authenticationService, times(1)).findUserByEmail(email);
		verify(messageSource, times(1)).getMessage("duplicated_email.error.msg", null, locale);
	}

	@Test
	public void testSignUp(){
		String email = "test@test.com";
		String password = "password";
		Locale locale = Locale.GERMAN;
		RedirectAttributes model = mock(RedirectAttributes.class);
		when(messageSource.getMessage("sign_up_success.msg", null, locale)).thenReturn("msg");
		String result = controller.signUp(email, password, locale, model);
		assertEquals(result, Constants.REDIRECT_TO_LOGIN_PAGE);
		verify(authenticationService, times(1)).signUp(email, password, locale);
		verify(messageSource, times(1)).getMessage("sign_up_success.msg", null, locale);
	}

	@Test
	public void testSignUpApi(){
		String email = "test@test.com";
		String password = "password";
		Locale locale = Locale.GERMAN;
		when(messageSource.getMessage("sign_up_success.msg", null, locale)).thenReturn("msg");
		Map<String, String> result = controller.signUpApi(email, password, locale);
		assertEquals(result.get(Constants.SUCCESS), Boolean.TRUE.toString());
		verify(authenticationService, times(1)).signUp(email, password, locale);
		verify(messageSource, times(1)).getMessage("sign_up_success.msg", null, locale);
	}

	@Test
	public void testSignUpApi_alreadyExist(){
		String email = "test@test.com";
		String password = "password";
		Locale locale = Locale.GERMAN;
		User user = new User();
		when(authenticationService.findUserByEmail(email)).thenReturn(user);
		when(messageSource.getMessage("duplicated_email.error.msg", null, locale)).thenReturn("msg");
		Map<String, String> result = controller.signUpApi(email, password, locale);
		assertEquals(result.get(Constants.SUCCESS), Boolean.FALSE.toString());
		verify(authenticationService, times(1)).findUserByEmail(email);
		verify(messageSource, times(1)).getMessage("duplicated_email.error.msg", null, locale);
	}

	@Test(expected = ResourceNotFoundException.class)
	public void testConfirmEmail_notFound(){
		RedirectAttributes model = mock(RedirectAttributes.class);
		controller.confirmEmail(model, "token", Locale.FRENCH);
	}

	@Test
	public void testConfirmEmail(){
		RedirectAttributes model = mock(RedirectAttributes.class);
		User user = new User();
		when(authenticationService.getUserByConfirmationToken("token")).thenReturn(user);
		String result = controller.confirmEmail(model, "token", Locale.FRENCH);
		assertEquals(result, Constants.REDIRECT_TO_LOGIN_PAGE);
		verify(authenticationService, times(1)).save(user);
	}

	@Test
	public void testGetAllUsers(){
		controller.getUsers();
		verify(authenticationService, times(1)).getAllUsers();
	}
}
