package toptal.user;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import toptal.model.user.User;
import toptal.model.user.UserRole;
import toptal.repositories.UserRepository;
import toptal.user.services.AuthUserDetails;

import static junit.framework.TestCase.assertNotNull;
import static org.mockito.Mockito.*;

/**
 * User: Maksym Goroshkevych Date: 31.05.14 Time: 20:06
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthUserDetailsTest
{
	@Mock
	private UserRepository userRepository;

	@InjectMocks
	private AuthUserDetails authUserDetails;

	private String email = "test@test.com";

	@Test
	public void testLoadUserByUsername(){
		User user = new User();
		user.setEmail(email);
		user.setPassword("password");
		user.setRole(UserRole.ROLE_USER);
		user.setConfirmationToken("token");
		user.setEnabled(true);
		user.setName("name");
		when(userRepository.findByEmail(email)).thenReturn(user);
		UserDetails userDetails = authUserDetails.loadUserByUsername(email);
		assertNotNull(userDetails);
		verify(userRepository, times(1)).findByEmail(email);
		reset(userRepository);
	}

	@Test(expected = UsernameNotFoundException.class)
	public void testLoadUserByUsername_notFound (){
		authUserDetails.loadUserByUsername(email);
	}
}
