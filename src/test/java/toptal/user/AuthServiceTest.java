package toptal.user;

import com.google.common.collect.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import toptal.common.EmailService;
import toptal.model.user.User;
import toptal.repositories.UserRepository;
import toptal.user.services.AuthService;

import javax.servlet.ServletContext;
import java.util.List;
import java.util.Locale;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

/**
 * User: Maksym Goroshkevych Date: 31.05.14 Time: 15:54
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthServiceTest
{
	@Mock
	private UserRepository userRepository;

	@Mock
	private EmailService emailService;

	@Mock
	private ServletContext servletContext;

	@InjectMocks
	private AuthService authService;

	@Test
	public void testSignUp(){
		authService.setEncoder(new StandardPasswordEncoder());
		when(servletContext.getContextPath()).thenReturn("test");
		authService.signUp("test@test.com", "123456", Locale.CANADA);
		verify(emailService, times(1)).sendEmail(anyString(), anyString(), any(Locale.class), any());
	}

	@Test
	public void testGetUserByConfirmationToken(){
		String token = "token";
		authService.getUserByConfirmationToken(token);
		verify(userRepository, times(1)).findByConfirmationToken(token);
	}

	@Test
	public void testSave(){
		User user = new User();
		authService.save(user);
		verify(userRepository, times(1)).save(user);
	}

	@Test
	public void testFindUserByEmail(){
		String email = "test@test.com";
		authService.findUserByEmail(email);
		verify(userRepository, times(1)).findByEmail(email);
	}

	@Test
	public void testGetAllUsers(){
		Iterable<User> users = Lists.newArrayList(new User());
		when(userRepository.findAll()).thenReturn(users);
		List<User> result = authService.getAllUsers();
		assertEquals(result.size(), 1);
		verify(userRepository, times(1)).findAll();
	}
}
