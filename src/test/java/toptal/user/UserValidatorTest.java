package toptal.user;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import toptal.common.exceptions.ValidationException;
import toptal.user.UserValidator;

import java.util.Locale;

/**
 * User: Maksym Goroshkevych Date: 31.05.14 Time: 20:25
 */
@RunWith(MockitoJUnitRunner.class)
public class UserValidatorTest
{
	@Mock
	private MessageSource messageSource;

	@InjectMocks
	private UserValidator validator;

	private Locale locale = Locale.CHINA;

	@Test(expected = ValidationException.class)
	public void testValidatePassword_null(){
		validator.validatePassword(null, locale);
	}

	@Test(expected = ValidationException.class)
	public void testValidatePassword_shortPassword(){
		validator.validatePassword("pass", locale);
	}

	@Test(expected = ValidationException.class)
	public void testValidateEmail_empty(){
		validator.validateEmail("", locale);
	}

	@Test(expected = ValidationException.class)
	public void testValidateEmail_notValid(){
		validator.validateEmail("test", locale);
	}

	@Test
	public void testValidate(){
		validator.validate("test@test.com", "12345567", locale);
	}
}
