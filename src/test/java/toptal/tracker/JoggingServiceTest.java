package toptal.tracker;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import toptal.model.tracker.Jogging;
import toptal.model.tracker.JoggingVO;
import toptal.model.user.User;
import toptal.repositories.JoggingRepository;
import toptal.tracker.services.JoggingMapper;
import toptal.tracker.services.JoggingService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static junit.framework.Assert.assertFalse;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class JoggingServiceTest {

	@Mock
	private JoggingRepository repository;

	@Mock
	private JoggingMapper mapper;

	@Mock
	private MessageSource messageSource;

	@InjectMocks
	private JoggingService service;

	@After
	public void cleanUp(){
		reset(repository, mapper, messageSource);
	}

	@Test
	public void testGetUserJogging(){
		User user = new User();
		service.getUserJogging(user, 1, 5, "id", Sort.Direction.ASC, "~");
		verify(repository,  times(1)).findUserJogging(anyLong(), any(Date.class), any(Date.class), any(Pageable.class));
	}

	@Test
	public void testGetJogging(){
		User user = new User();
		service.getJogging(user, 1L);
		verify(repository,  times(1)).findByIdAndUserId(1L, null);
	}

	@Test
	public void testGetTotalReportData(){
		List<Object[]> data = new ArrayList<>();
		when(repository.getReportData(1L)).thenReturn(data);
		service.getTotalReportData(1L);
		verify(repository, times(1)).getReportData(1L);
	}

	@Test
	public void testSaveJogging(){
		User user = new User();
		JoggingVO vo = new JoggingVO();
		Jogging jogging = new Jogging();
		when(mapper.map(vo)).thenReturn(jogging);
		Locale locale = Locale.ENGLISH;
		when(messageSource.getMessage("record_was_saved.msg", null, locale)).thenReturn("");
		Map<String, String> result = service.saveJogging(user, vo, locale);
		assertTrue(result.get("success").equals("true"));
		verify(mapper, times(1)).map(vo);
		verify(repository, times(1)).save(jogging);
		verify(messageSource, times(1)).getMessage("record_was_saved.msg", null, locale);
	}

	@Test
	public void testEditJogging(){
		User user = new User();
		JoggingVO vo = new JoggingVO();
		Jogging jogging = new Jogging();
		when(mapper.map(vo)).thenReturn(jogging);
		Locale locale = Locale.ENGLISH;
		when(repository.findByIdAndUserId(null, null)).thenReturn(jogging);
		when(messageSource.getMessage("record_was_saved.msg", null, locale)).thenReturn("");
		Map<String, String> result = service.editJogging(user, vo, locale);
		assertTrue(result.get("success").equals("true"));
		verify(mapper, times(1)).map(vo);
		verify(repository, times(1)).save(jogging);
		verify(messageSource, times(1)).getMessage("record_was_saved.msg", null, locale);
	}

	@Test
	public void testEditJogging_notFound(){
		User user = new User();
		JoggingVO vo = new JoggingVO();
		Jogging jogging = new Jogging();
		Locale locale = Locale.ENGLISH;
		when(messageSource.getMessage("record.was_not_found.error_msg", null, locale)).thenReturn("");
		Map<String, String> result = service.editJogging(user, vo, locale);
		assertFalse(result.get("success").equals("true"));
		verify(mapper, times(0)).map(vo);
		verify(repository, times(1)).findByIdAndUserId(null, null);
		verify(repository, times(0)).save(jogging);
		verify(messageSource, times(1)).getMessage("record.was_not_found.error_msg", null, locale);
	}

	@Test
	public void testDeleteJogging(){
		User user = new User();
		Jogging jogging = new Jogging();
		Locale locale = Locale.ENGLISH;
		when(repository.findByIdAndUserId(null, null)).thenReturn(jogging);
		when(messageSource.getMessage("record_was_removed.msg", null, locale)).thenReturn("");
		Map<String, String> result = service.deleteJogging(user, null, locale);
		assertTrue(result.get("success").equals("true"));
		verify(repository, times(1)).delete(jogging);
		verify(messageSource, times(1)).getMessage("record_was_removed.msg", null, locale);
	}

	@Test
	public void testDeleteJogging_notFound(){
		User user = new User();
		Jogging jogging = new Jogging();
		Locale locale = Locale.ENGLISH;
		when(messageSource.getMessage("record.was_not_found.error_msg", null, locale)).thenReturn("");
		Map<String, String> result = service.deleteJogging(user, null, locale);
		assertFalse(result.get("success").equals("true"));
		verify(repository, times(1)).findByIdAndUserId(null, null);
		verify(repository, times(0)).delete(jogging);
		verify(messageSource, times(1)).getMessage("record.was_not_found.error_msg", null, locale);
	}
}
