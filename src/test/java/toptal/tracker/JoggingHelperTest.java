package toptal.tracker;

import org.junit.Test;
import toptal.tracker.helpers.JoggingHelper;

import static junit.framework.TestCase.assertEquals;

public class JoggingHelperTest {

	private JoggingHelper helper = new JoggingHelper();

	@Test
	public void testGetSortColumn(){
		assertEquals(helper.getSortColumn(0), "date");
		assertEquals(helper.getSortColumn(1), "time");
		assertEquals(helper.getSortColumn(2), "distance");
		assertEquals(helper.getSortColumn(3), "id");
		assertEquals(helper.getSortColumn(4), "comment");
		assertEquals(helper.getSortColumn(5), "id");
	}
}
