package toptal.tracker;

import com.google.common.collect.Lists;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.util.MultiValueMap;
import toptal.common.TableHelper;
import toptal.model.tracker.Jogging;
import toptal.model.tracker.JoggingVO;
import toptal.model.user.User;
import toptal.tracker.controllers.JoggingApiController;
import toptal.tracker.helpers.JoggingHelper;
import toptal.tracker.services.JoggingMapper;
import toptal.tracker.services.JoggingService;
import toptal.tracker.validators.JoggingValidator;
import toptal.user.services.LoggedUserService;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class JoggingApiControllerTest {

	@Mock
	private JoggingService joggingService;

	@Mock
	private LoggedUserService userService;

	@Mock
	private JoggingHelper joggingHelper;

	@Mock
	private JoggingMapper mapper;

	@Mock
	private TableHelper tableHelper;

	@Mock
	private JoggingValidator validator;

	@InjectMocks
	private JoggingApiController controller;

	private User user = new User();
	private Locale locale = Locale.ENGLISH;

	@Before
	public void init(){
		when(userService.getUser()).thenReturn(user);
	}

	@After
	public void cleanUp(){
		reset(joggingService, userService, joggingHelper, mapper, tableHelper, validator);
	}

	@Test
	public void testGetJoggingList() throws IOException {
		when(joggingHelper.getSortColumn(1)).thenReturn("id");
		Page<Jogging> page = mock(Page.class);
		when(joggingService.getUserJogging(user, 0, 10, "id", Sort.Direction.ASC, "")).thenReturn(page);
		String response = "response";
		when(tableHelper.convert(page)).thenReturn(response);
		String result = controller.getJoggingList(0, "", 1, "asc", 10);
		verify(joggingHelper, times(1)).getSortColumn(1);
		verify(tableHelper, times(1)).convert(page);
		assertEquals(result, response);
	}

	@Test
	public void testGetJoggingJsonList() throws IOException {
		when(joggingHelper.getSortColumn(1)).thenReturn("id");
		Page<Jogging> page = mock(Page.class);
		when(joggingService.getUserJogging(user, 0, 10, "id", Sort.Direction.ASC, "")).thenReturn(page);
		List<Jogging> response = Lists.newArrayList();
		when(page.getContent()).thenReturn(response);
		List<Jogging> result = controller.getJoggingJsonList(0, "", 1, "asc", 10);
		verify(joggingHelper, times(1)).getSortColumn(1);
		verify(page, times(1)).getContent();
		assertEquals(result, response);
	}


	@Test
	public void testGetJogging() throws IOException {
		Jogging response = new Jogging();
		when(joggingService.getJogging(user, 5L)).thenReturn(response);
		Jogging result = controller.getJogging(5L);
		verify(joggingService, times(1)).getJogging(user, 5L);
		assertEquals(result, response);
	}

	@Test
	public void testSaveJogging() throws IOException {
		JoggingVO vo = new JoggingVO();
		Map<String, String> response = new HashMap<>();
 		when(joggingService.saveJogging(user, vo, locale)).thenReturn(response);
		Map<String, Object> result = controller.saveJogging(vo, locale);
		verify(joggingService, times(1)).saveJogging(user, vo, locale);
		verify(validator, times(1)).validate(vo, locale);
		assertEquals(result, response);
	}

	@Test
	public void testEditJogging() throws IOException {
		MultiValueMap<String, String> params = mock(MultiValueMap.class);
		JoggingVO vo = new JoggingVO();
		when(mapper.map(params)).thenReturn(vo);
		Map<String, String> response = new HashMap<>();
		when(joggingService.editJogging(user, vo, locale)).thenReturn(response);
		Map<String, Object> result = controller.editJogging(null, params, locale);
		verify(joggingService, times(1)).editJogging(user, vo, locale);
		verify(validator, times(1)).validate(vo, locale);
		verify(mapper, times(1)).map(params);
		assertEquals(result, response);
	}

	@Test
	public void testDelete() throws IOException {
		Map<String, String> response = new HashMap<>();
		when(joggingService.deleteJogging(user, 5L, locale)).thenReturn(response);
		Map<String, Object> result = controller.delete(5L, locale);
		verify(joggingService, times(1)).deleteJogging(user, 5L, locale);
		assertEquals(result, response);
	}

}
