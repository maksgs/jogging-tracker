package toptal.tracker;

import com.google.common.collect.Lists;
import com.itextpdf.text.DocumentException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import toptal.model.tracker.JoggingReportItem;
import toptal.model.user.User;
import toptal.tracker.helpers.ReportHelper;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Locale;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ReportHelperTest {

	@Mock
	private MessageSource messageSource;

	@InjectMocks
	private ReportHelper helper;

	@Test
	public void testGenerateReport() throws IOException, DocumentException {
		HttpServletResponse response = mock(HttpServletResponse.class);
		User user = new User();
		user.setEmail("test@test.com");
		Locale locale = Locale.ENGLISH;
		JoggingReportItem reportItem = new JoggingReportItem();
		reportItem.setYear(2014);
		reportItem.setWeek(14);
		reportItem.setTimes(1L);
		reportItem.setAverageSpeed(15D);
		reportItem.setAverageDistance(3000L);
		reportItem.setTotalDistance(3000L);
		reportItem.setTotalTime(720L);
		reportItem.setAverageTime(720L);
		when(messageSource.getMessage(anyString(), any(), any(Locale.class))).thenReturn("");
		try{
			helper.generateReport(response, user, locale, Lists.newArrayList(reportItem));
		}catch (NullPointerException ex){
			//ignore
		}
		verify(messageSource, times(1)).getMessage("report.title", null, locale);
		verify(messageSource, times(1)).getMessage("report.generated.by", null, locale);
		verify(messageSource, times(1)).getMessage("week", null, locale);
		verify(messageSource, times(1)).getMessage("year", null, locale);
		verify(messageSource, times(1)).getMessage("total.distance", null, locale);
		verify(messageSource, times(1)).getMessage("total.time", null, locale);
		verify(messageSource, times(1)).getMessage("times", null, locale);
		verify(messageSource, times(1)).getMessage("average.speed", null, locale);
		verify(messageSource, times(1)).getMessage("average.distance", null, locale);
		verify(messageSource, times(1)).getMessage("average.time", null, locale);

		verify(response, times(1)).setContentType("text/pdf;charset=utf-8");
	}
}
