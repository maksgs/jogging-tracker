package toptal.tracker;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import toptal.model.tracker.JoggingVO;
import toptal.tracker.validators.JoggingValidator;

import java.util.Locale;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class JoggingValidatorTest {

	@Mock
	private MessageSource messageSource;

	@InjectMocks
	private JoggingValidator validator;

	private Locale locale = Locale.ENGLISH;

	private static final String MSG = "msg";

	@After
	public void cleanUp(){
		reset(messageSource);
	}

	@Test
	public void testValidate_emptyDate(){
		JoggingVO vo = initData();
		vo.setDate("");
		when(messageSource.getMessage("date_could_not_be_null.error.msg", null, locale)).thenReturn(MSG);
		String result = validator.validate(vo, locale);
		verify(messageSource, times(1)).getMessage("date_could_not_be_null.error.msg", null, locale);
		assertEquals(result, MSG);
	}

	@Test
	public void testValidate_emptyTime(){
		JoggingVO vo = initData();
		vo.setTime("");
		when(messageSource.getMessage("time_could_not_be_null.error.msg", null, locale)).thenReturn(MSG);
		String result = validator.validate(vo, locale);
		verify(messageSource, times(1)).getMessage("time_could_not_be_null.error.msg", null, locale);
		assertEquals(result, MSG);
	}

	@Test
	public void testValidate_distance0(){
		JoggingVO vo = initData();
		vo.setDistance(0);
		when(messageSource.getMessage("distance_could_not_be_null.error.msg", null, locale)).thenReturn(MSG);
		String result = validator.validate(vo, locale);
		verify(messageSource, times(1)).getMessage("distance_could_not_be_null.error.msg", null, locale);
		assertEquals(result, MSG);
	}


	@Test
	public void testValidate_notValidTime(){
		JoggingVO vo = initData();
		vo.setTime("0:00:00");
		when(messageSource.getMessage("time_is_not_valid.error.msg", null, locale)).thenReturn(MSG);
		String result = validator.validate(vo, locale);
		verify(messageSource, times(1)).getMessage("time_is_not_valid.error.msg", null, locale);
		assertEquals(result, MSG);
	}

	@Test
	public void testValidate_notValidTime_parseException(){
		JoggingVO vo = initData();
		vo.setTime("a:aa:00");
		when(messageSource.getMessage("time_is_not_valid.error.msg", null, locale)).thenReturn(MSG);
		String result = validator.validate(vo, locale);
		verify(messageSource, times(1)).getMessage("time_is_not_valid.error.msg", null, locale);
		assertEquals(result, MSG);
	}

	@Test
	public void testValidate_notValidDate(){
		JoggingVO vo = initData();
		vo.setDate("2014-11-11");
		when(messageSource.getMessage("date_is_not_valid.error.msg", null, locale)).thenReturn(MSG);
		String result = validator.validate(vo, locale);
		verify(messageSource, times(1)).getMessage("date_is_not_valid.error.msg", null, locale);
		assertEquals(result, MSG);
	}

	@Test
	public void testValidate_success(){
		JoggingVO vo = initData();
		String result = validator.validate(vo, locale);
		assertNull(result);
	}

	private JoggingVO initData(){
		JoggingVO vo = new JoggingVO();
		vo.setComment("comment");
		vo.setDistance(100);
		vo.setTime("00:10:21");
		vo.setDate("01-05-2010");
		return vo;

	}
}
