package toptal.tracker;

import org.junit.Test;
import org.springframework.ui.Model;
import toptal.model.tracker.JoggingVO;
import toptal.tracker.controllers.DashboardController;

import static junit.framework.TestCase.assertNotNull;
import static org.mockito.Mockito.*;

public class DashboardControllerTest {

	private DashboardController controller = new DashboardController();

	@Test
	public void testGetHomePage(){
		Model model = mock(Model.class);
		assertNotNull(controller.getDashboardPage(model));
		verify(model, times(1)).addAttribute(anyString(), any(JoggingVO.class));
	}
}
