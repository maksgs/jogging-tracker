package toptal.tracker;

import com.google.common.collect.Lists;
import com.itextpdf.text.DocumentException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import toptal.model.user.User;
import toptal.tracker.controllers.ReportController;
import toptal.tracker.helpers.ReportHelper;
import toptal.tracker.services.JoggingService;
import toptal.user.services.LoggedUserService;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Locale;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ReportControllerTest {

	@InjectMocks
	private ReportController controller;

	@Mock
	private ReportHelper reportHelper;

	@Mock
	private LoggedUserService userService;

	@Mock
	private JoggingService joggingService;

	@Test
	public void testGenerateReport() throws IOException, DocumentException {
		HttpServletResponse response = mock(HttpServletResponse.class);
		Locale locale = Locale.ENGLISH;
		User user = new User();
		when(userService.getUser()).thenReturn(user);
		controller.getDashboardPage(response, locale);
		verify(reportHelper, times(1)).generateReport(response, user, locale, Lists.newArrayList());
		verify(userService, times(1)).getUser();
		verify(joggingService, times(1)).getTotalReportData(null);
	}

}
