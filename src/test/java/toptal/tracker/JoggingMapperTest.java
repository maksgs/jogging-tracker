package toptal.tracker;

import com.google.common.collect.Lists;
import org.junit.Test;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import toptal.model.tracker.Jogging;
import toptal.model.tracker.JoggingReportItem;
import toptal.model.tracker.JoggingVO;
import toptal.tracker.services.JoggingMapper;

import java.math.BigInteger;

import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertEquals;

public class JoggingMapperTest {

	private JoggingMapper mapper = new JoggingMapper();

	@Test
	public void testMapVoToEntity(){
		JoggingVO vo = initData();
		Jogging jogging = mapper.map(vo);
		assertEquals(vo.getDistance(), jogging.getDistance());
		assertEquals(vo.getComment(), jogging.getComment());
		assertEquals(vo.getDate(), jogging.getDate());
	}

	@Test
	public void testMapParamsToEntity(){
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.put("distance", Lists.newArrayList ("100"));
		params.put("date", Lists.newArrayList ("01-05-2010"));
		params.put("time", Lists.newArrayList ("00:10:21"));
		params.put("comment", Lists.newArrayList ("comment"));
		JoggingVO expectData = initData();
		JoggingVO jogging = mapper.map(params);
		assertEquals(expectData.getDistance(), jogging.getDistance());
		assertEquals(expectData.getComment(), jogging.getComment());
		assertEquals(expectData.getTime(), jogging.getTime());
		assertEquals(expectData.getDate(), jogging.getDate());
	}

	@Test
	public void testDBObjectToJoggingReportItem(){
		Object[] dbData = new Object[]{2014D, 58D, new BigInteger("720"), new BigInteger("3000"), new BigInteger("1")};
		JoggingReportItem reportItem = mapper.map(dbData);
		assertTrue(1L == reportItem.getTimes());
		assertTrue(3000L == reportItem.getAverageDistance());
		assertTrue(3000L == reportItem.getTotalDistance());
		assertTrue(15L == reportItem.getAverageSpeed());
		assertTrue(720L == reportItem.getTotalTime());
		assertTrue(720L == reportItem.getAverageTime());
		assertTrue(2014L == reportItem.getYear());
		assertTrue(58 == reportItem.getWeek());
	}

	private JoggingVO initData(){
		JoggingVO vo = new JoggingVO();
		vo.setComment("comment");
		vo.setDistance(100);
		vo.setTime("00:10:21");
		vo.setDate("01-05-2010");
		return vo;

	}
}
